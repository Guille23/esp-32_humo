import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, set } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

const firebaseConfig = {
    apiKey: "AIzaSyA-DniDgS0hQeZK3SuqmnZH0fuh-jVqNjA",
    authDomain: "tp-final-ac9a1.firebaseapp.com",
    databaseURL: "https://tp-final-ac9a1-default-rtdb.firebaseio.com",
    projectId: "tp-final-ac9a1",
    storageBucket: "tp-final-ac9a1.appspot.com",
    messagingSenderId: "923046997669",
    appId: "1:923046997669:web:f478e3498c397c9299a74c",
    measurementId: "G-3R519KWGK5"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig)

// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(app)

const database = getDatabase(app)

let correoRefLog = document.getElementById("Email-Login");
let passRefLog = document.getElementById("Password-Login");
let buttonRefLog = document.getElementById("Boton-Login");

buttonRefLog.addEventListener("click", LogIn);

//funcion para Iniciar sesion

function LogIn (){

    if((correoRefLog.value != '') && (passRefLog.value != '')){

        signInWithEmailAndPassword(auth, correoRefLog.value, passRefLog.value)
        .then((userCredential) => {
            const user = userCredential.user;
            window.location.href = "../index.html";
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        });
    }
    else{
        alert("Revisar que los campos de usuario y contraseña esten completos.");
    }    

}
