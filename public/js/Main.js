// importar funciones de firebase
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getDatabase, ref, set, push, onValue, orderByKey, query  } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";
import { getAuth, onAuthStateChanged, signOut } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

const firebaseConfig = {
  apiKey: "AIzaSyA-DniDgS0hQeZK3SuqmnZH0fuh-jVqNjA",
  authDomain: "tp-final-ac9a1.firebaseapp.com",
  databaseURL: "https://tp-final-ac9a1-default-rtdb.firebaseio.com",
  projectId: "tp-final-ac9a1",
  storageBucket: "tp-final-ac9a1.appspot.com",
  messagingSenderId: "923046997669",
  appId: "1:923046997669:web:f478e3498c397c9299a74c",
  measurementId: "G-3R519KWGK5"
};

// var UID;
// var correo;

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth();
const user = auth.currentUser;

let TempRef = document.getElementById("Temp")

let botonOnRef = document.getElementById("incr")
let botonOffRef = document.getElementById("decr")
let AlarmaRef = document.getElementById("Alarma")
let ulHeaderRef = document.getElementById("ul-header")
// let logoutRef = document.getElementById("logout")

var correo;
var UID;

botonOnRef.addEventListener("click", EnviarTrue)
botonOffRef.addEventListener("click", EnviarFalse)
// logoutRef.addEventListener("click", Logout)

function Logout(){
  signOut(auth).then(() => {
    // Sign-out successful.
    location.reload()
  }).catch((error) => {
    // An error happened.
  });
}

function EnviarTrue(){
  set(ref(database, "OnOff/"), {
    Estado: true,

})
}

const RutaRef = ref(database, 'Sensor/');
onValue(RutaRef, (snapshot) => {
  const data = snapshot.val();
  TempRef.innerHTML = `
  <p style="font-size: 100px;">${data}</p>
  `;
})

function EnviarFalse(){
  set(ref(database, "OnOff/"), {
    Estado: false,

})
}

const ON_OFFRef = ref(database, '/OnOff/Estado');
onValue(ON_OFFRef, (snapshot) => {
  const dataON_OFF = snapshot.val();
  if (dataON_OFF == true){
    var dataOnOFF1 = "Estado: Encendido"
    
  }
  else{
    var dataOnOFF1 = "Estado: Apagado"
  }
  // AlarmaRef.innerHTML = ""
  AlarmaRef.innerHTML = `
  <p style = " font-size : 13px;" >${dataOnOFF1}</p>
  `;
})

onAuthStateChanged(auth, (user) => {
  if (user) {
    const uid = user.uid;
    UID = uid;
    console.log("Usuario actual:" + uid);

    const email = user.email;
    correo=email;
    console.log(email)
    botonOnRef.addEventListener("click", EnviarTrue)
    botonOffRef.addEventListener("click", EnviarFalse)
    ulHeaderRef.innerHTML = `
    <li class="nav-item">
            <button class="btn btn-outline-warning me-3" type="button" id="logout">Cerrar sesión</button>
        </li>
    `
    let logoutRef = document.getElementById("logout")
    logoutRef.addEventListener("click", Logout)
  } else {
    console.log("Ningun usuario detectado ")
    botonOnRef.addEventListener("click", Ventana)
  botonOffRef.addEventListener("click", Ventana)
  }
});

function Ventana(){
  window.location.href = "pages/Login.html"
}
