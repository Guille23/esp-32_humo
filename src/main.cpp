//Incluimos las librerias 
#include <Arduino.h>
#include <WiFi.h>
#include <FirebaseESP32.h>

 //Defimos unas 
#define FIREBASE_HOST "esp32firebase-af667-default-rtdb.firebaseio.com"
#define FIREBASE_AUTH "AIzaSyD4V-l760_muhq7eUuCVW171vSyA23CFkU"

FirebaseData TPFinal;
//Definimos una ruta de Firebase
String ruta = "Sensor" ;
String rutaEstado = "OnOff/Estado/" ;

//Red a cual conectarse 
#define WIFI_SSID "TeoEsp32"
#define WIFI_PASSWORD "123456789"
#define LED 2 

#define PinAnalogico 26  
#define PinDigital 22
#define Buzzer 23


int limite;
int valor;

void setup() {

Serial.begin(115200);//velocidad de baudios

pinMode(PinDigital, INPUT);//establecer al pin como entrada
pinMode(Buzzer, OUTPUT);//establecer al pin como salida

WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
Serial.print("Se esta conectando a:");
Serial.println(WIFI_SSID);

while (WiFi.status() != WL_CONNECTED)
{
  delay(500);
  Serial.print(".");
}

Serial.println("");
Serial.println("Wifi conectado");
Serial.println("IP: ");
Serial.println(WiFi.localIP());

Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
Firebase.reconnectWiFi(true);

}
void loop(){

int valor= analogRead(PinAnalogico);//lee el valor analogico del pin A0
limite= digitalRead(PinDigital);//lee el valor digital del pin D8
Serial.print("Valor del CO: ");
Serial.println(valor);//imprime los valores de CO
Serial.print("Limite: ");
Serial.print(limite);//imprime el limete alcanzado ALTO/BAJO
delay(100);

Firebase.setInt(TPFinal, ruta, valor);
Firebase.setInt(TPFinal, rutaEstado, limite);

Firebase.getBool(TPFinal, rutaEstado);

Serial.println(TPFinal.intData());
if (TPFinal.intData() == 1)
{
  digitalWrite(Buzzer, HIGH);
}
else{
  digitalWrite(Buzzer, LOW);
}
}
